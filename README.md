# CouchDB dockerization example

## Run
#### Add to etc/hosts (sudo nano /etc/hosts):
```
127.0.0.1 couchdb-test.vextasy.local
```

#### Run tests with command:  
```bash
docker-compose up --build --remove-orphans  
```

#### Look into the database:
#### Open in browser:
```
https://couchdb-test.vextasy.local/_utils/
```

## Explore couchdb API:
```
https://docs.couchdb.org/en/stable/intro/api.html
```

